import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;
import processing.event.MouseEvent;

public class Main extends PApplet {

    //private ArrayList<Bird> birds = new ArrayList<>();
    private int counter = 1;
    private int drawCounter = 0;
    private PVector mousePos = new PVector(0,0);
    public SpatialGrid grid;
    private int previousMillis = 0;

    public void settings(){
        size(1000, 600);
        grid = new SpatialGrid(this,50f);

        PImage image = loadImage("src\\main\\resources\\images\\bird_sprite.png");
        // Create a bunch of birds, at random positions
        for (int i=0; i<20; ++i) {
            PVector randomPosition = new PVector(random(0,width), random(0,height));
            Bird bird = new Bird(this, randomPosition,new Sprite(image,this));
            bird.update(random(0,1));
            grid.birds.add(bird);
        }



    }

    public void draw(){

        // Reassigning these here to allow them to be modified in Tweak Mode
        SpatialGrid.BIRD_MAX_SPEED = 200;
        SpatialGrid.BIRD_MOUSE_FOLLOW_STRENGTH = 100;
        SpatialGrid.BIRD_SEPARATION_RADIUS = 38;
        SpatialGrid.BIRD_SEPARATION_STRENGTH = 250;
        SpatialGrid.BIRD_ALIGNMENT_RADIUS = 60;
        SpatialGrid.BIRD_ALIGNMENT_STRENGTH = 200;
        SpatialGrid.OBSTACLE_SIZE = 120;
        SpatialGrid.OBSTACLE_AVOID_STRENGTH = 3000;

        // Calculate delta time since last frame
        int millisElapsed = millis() - previousMillis;
        float secondsElapsed = millisElapsed / 1000f;
        previousMillis = millis();

        background(10);
        fill(89, 169, 249,200);
        rect(-5,-5,width+5,height+5);

        grid.empty();
        for (Bird bird : grid.birds) {
            grid.add(bird, bird.position.x, bird.position.y);
        }
        for (Predator predator : grid.predators) {
            grid.add(predator, predator.position.x, predator.position.y);
        }


        // Draw obstacles

        for (Obstacle obstacle : grid.obstacles) {
            obstacle.draw();
        }

        // Calculate forces on birds
        for (Bird bird : grid.birds) {
            bird.claculateAcceleration(grid);
        }

        // draw birds
        for (Bird bird : grid.birds) {
            bird.update(secondsElapsed);
            bird.draw();
        }

        // Calculate forces on predators
        for (Predator predator : grid.predators) {
            predator.claculateAcceleration(grid);
        }

        // draw predators
        for (Predator predator : grid.predators) {
            predator.update(secondsElapsed);
            predator.draw();
        }

    }

    @Override
    protected void handleMouseEvent(MouseEvent event) {
        super.handleMouseEvent(event);
        final int action = event.getAction();
        if (action == MouseEvent.CLICK)
        {
            pmouseX = emouseX;
            pmouseY = emouseY;
            mouseX = event.getX();
            mouseY = event.getY();
            mousePos = new PVector(mouseX,mouseY);
        }

    }

    public static void main(String... args){
        String[] processingArgs = {"MySketch"};
        Main mySketch = new Main();
        PApplet.runSketch(processingArgs, mySketch);
    }

    public PVector getMousePos() {
        return mousePos;
    }

    public void setMousePos(PVector mousePos) {
        this.mousePos = mousePos;
    }
}
