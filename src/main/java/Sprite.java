import processing.core.PImage;

public class Sprite {

    private PImage spriteSheet;
    private Main sketch;

    // Hardcoded values to match the bird spritesheet
    int frameCount = 3;
    float frameDurationSeconds = 0.1f;
    int frameWidth = 31;

    float secondsSinceAnimationStarted = 0;

    public Sprite(PImage spriteSheet, Main sketch) {
        this.spriteSheet = spriteSheet;
        this.sketch = sketch;
    }

    void updateAnimation(float secondsElapsed) {
        secondsSinceAnimationStarted += secondsElapsed;
    }

    void draw() {
        // Figure out which frame we should be drawing currently
            int currentFrameIndex = (int)(secondsSinceAnimationStarted / frameDurationSeconds);
        currentFrameIndex %= frameCount;

        drawAnimationFrame(currentFrameIndex);
    }

    void drawAnimationFrame(int frameIndex) {
        // Crop out and draw the appropriate part of the image
        sketch.imageMode(sketch.CENTER);

        int frameStartX = frameWidth*frameIndex;

        sketch.image(
                spriteSheet,
                0, 0,
                frameWidth, frameWidth,                // Size to draw
                frameStartX, 0,                        // Top-left of section to draw
                frameStartX + frameWidth, frameWidth   // Bottom-right
        );
    }
}
