import processing.core.PImage;
import processing.core.PVector;

public class Obstacle{

    private Main sketch;
    private PImage image;
    private PVector position;

    public Obstacle(Main sketch, PVector position){
        this.sketch = sketch;
        this.position = position;
    }

    public void draw(){
        image = sketch.loadImage("src\\main\\resources\\images\\tree.png");
        sketch.image(image,position.x,position.y);
    }

    public PVector getPosition() {
        return position;
    }
}
