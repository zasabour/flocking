import processing.core.PImage;
import processing.core.PVector;

public class Agent {
    private Main sketch;
    private Sprite sprite;
    protected PVector position;
    protected PVector velocity = new PVector(0,0);

    private PVector acceleration = new PVector(0,0);
    private SpatialGrid grid;

    // Most recent flocking forces
    private PVector separationForce = new PVector(0,0);
    private PVector alignmentForce = new PVector(0,0);

    public Agent(Main sketch, PVector position, Sprite sprite){
        this.sketch = sketch;
        this.position = position;
        this.sprite = sprite;
        grid = sketch.grid;
    }

    public void update(float secondsElapsed) {
        sprite.updateAnimation(secondsElapsed);

        // Acceleration changes our velocity
        velocity.add(PVector.mult(acceleration, secondsElapsed));

        // Limit velocity
        if (velocity.magSq() > SpatialGrid.BIRD_MAX_SPEED*SpatialGrid.BIRD_MAX_SPEED) {
            velocity.setMag(SpatialGrid.BIRD_MAX_SPEED);
        }

        // Velocity changes our position
        position.add(PVector.mult(velocity, secondsElapsed));

        // Wrap birds around screen
        if (position.x >= sketch.width) { position.x -= sketch.width; }
        else if (position.x < 0) { position.x += sketch.width; }

        if (position.y >= sketch.height) { position.y -= sketch.height; }
        else if (position.y < 0) { position.y += sketch.height; }
    }



    public void claculateAcceleration(SpatialGrid grid){
        acceleration.set(0,0);

        // Steer towards mouse position
        PVector vectorToTarget = PVector.sub(sketch.getMousePos(), position);
        PVector accelerationTowardsTarget = vectorToTarget.setMag(SpatialGrid.BIRD_MOUSE_FOLLOW_STRENGTH);
        acceleration.add(accelerationTowardsTarget);

        /*
         * Avoid obstacles
         */
        for (Obstacle obstacle : grid.obstacles) {
            PVector vectorToObstacle = PVector.sub(obstacle.getPosition(), position);
            float squareDistanceToObstacle = vectorToObstacle.magSq();
            if (squareDistanceToObstacle < SpatialGrid.OBSTACLE_SIZE*SpatialGrid.OBSTACLE_SIZE) {
                float distanceToObstacle = (float)Math.sqrt(squareDistanceToObstacle);
                float obstacleAvoidAmount = ((1f-distanceToObstacle/SpatialGrid.OBSTACLE_SIZE) * (1f-distanceToObstacle/SpatialGrid.OBSTACLE_SIZE)) * SpatialGrid.OBSTACLE_AVOID_STRENGTH;
                acceleration.add(vectorToObstacle.setMag(-obstacleAvoidAmount));
            }
        }

        /*
         * SEPARATION - avoid my neighbours
         */
        separationForce.set(0,0);
        for (Agent otherBird : grid.query(position.x, position.y, SpatialGrid.BIRD_SEPARATION_RADIUS)) {
            if (otherBird == this) continue;   // Don't compare a bird with itself!

            PVector vectorToOtherBird = PVector.sub(otherBird.position, position);
            float squareDistanceToOtherBird = vectorToOtherBird.magSq();

            // Ignore if too far away
            if (squareDistanceToOtherBird > SpatialGrid.BIRD_SEPARATION_RADIUS*SpatialGrid.BIRD_SEPARATION_RADIUS) continue;

            // Repel from other bird!
            separationForce.add(vectorToOtherBird.setMag(-SpatialGrid.BIRD_SEPARATION_STRENGTH));
        }
        acceleration.add(separationForce);

        /*
         * ALIGNMENT - move in the same direction my neighbours are moving
         */
        alignmentForce.set(0,0);

        PVector averageVelocityOfNeighbours = new PVector(0,0);
        int alignmentNeighbourCount = 0;

        for (Agent otherBird : grid.query(position.x, position.y, SpatialGrid.BIRD_ALIGNMENT_RADIUS)) {
            if (otherBird == this) continue;   // Don't compare a bird with itself!

            PVector vectorToOtherBird = PVector.sub(otherBird.position, position);
            float squareDistanceToOtherBird = vectorToOtherBird.magSq();

            // Ignore if too far away
            if (squareDistanceToOtherBird > SpatialGrid.BIRD_ALIGNMENT_RADIUS*SpatialGrid.BIRD_ALIGNMENT_RADIUS) continue;

            // Accumulate average heading
            ++alignmentNeighbourCount;
            averageVelocityOfNeighbours.add(otherBird.velocity);
        }

        if (alignmentNeighbourCount > 0) {
            averageVelocityOfNeighbours.mult(1f / alignmentNeighbourCount);
            alignmentForce.set(averageVelocityOfNeighbours.setMag(SpatialGrid.BIRD_ALIGNMENT_STRENGTH));
            acceleration.add(alignmentForce);
        }


        /*
         * COHESION - Not implemented!
         */
    }

    public void draw() {
        sketch.pushMatrix();
        sketch.translate(position.x, position.y);

        float angle = sketch.atan2(velocity.y, velocity.x);
        sketch.rotate(angle);

        sprite.draw();
        sketch.popMatrix();
    }

    public void render(int counter){
        PImage image = sketch.loadImage("src\\main\\resources\\images\\bird_sprite.png");

        sketch.pushMatrix();
        sketch.translate(position.x+image.width/2,position.y +image.height/2);

        switch(counter)
        {
            case 1:
                image = image.get(0,0,31,29);
                counter++;
                break;
            case 2:
                image = image.get(33,0,31,29);
                counter++;
                break;
            case 3:
                image = image.get(65,0,31,29);
                counter = 0;
                break;
            default:
                image = image.get(65,0,31,29);
        }

        //sketch.rotate(xSpeed > 0 ? 0:sketch.PI);
        //sketch.rotate(ySpeed > 0? 0:sketch.PI);
        float angle = sketch.atan2(velocity.y, velocity.x);
        sketch.rotate(angle);
        //sketch.rotate(testCounter*TWO_PI/360);
        sketch.translate(-image.width/2,-image.height/2);
        sketch.image(image,position.x,position.y);
        sketch.popMatrix();

    }

}
