import processing.core.PImage;
import processing.core.PVector;

import java.util.ArrayList;

import static processing.core.PApplet.ceil;

public class SpatialGrid {

    private float cellSize;
    private int numCellsX, numCellsY;
    private ArrayList<ArrayList<Agent>> cells;

    public ArrayList<Bird> birds ;
    public ArrayList<Obstacle> obstacles;
    public ArrayList<Predator> predators;

    private Main sketch;

    static float BIRD_MAX_SPEED = 200;

    static float BIRD_MOUSE_FOLLOW_STRENGTH = 250;

    static float BIRD_SEPARATION_RADIUS = 65f;
    static float BIRD_SEPARATION_STRENGTH = 400f;

    static float BIRD_ALIGNMENT_RADIUS = 20f;
    static float BIRD_ALIGNMENT_STRENGTH = 200f;

    static float OBSTACLE_SIZE = 250;
    static float OBSTACLE_AVOID_STRENGTH = 3000f;

    public SpatialGrid(Main sketch, float _cellSize){
        this.cellSize = _cellSize;
        this.numCellsX = ceil(sketch.width/cellSize);
        this.numCellsY = ceil(sketch.height/cellSize);

        int numCellsTotal = numCellsX * numCellsY;
        cells = new ArrayList<ArrayList<Agent>>(numCellsTotal);
        for (int i=0; i<numCellsTotal; ++i) {
            cells.add(new ArrayList<Agent>());
        }
        this.sketch = sketch;

        birds = new ArrayList<Bird>();
        PImage image = sketch.loadImage("src\\main\\resources\\images\\bird_sprite.png");
        for (int i=0; i<20; ++i) {
            PVector randomPosition = new PVector(sketch.random(0,sketch.width), sketch.random(0,sketch.height));
            Bird bird = new Bird(sketch,randomPosition, new Sprite(image,sketch));
            bird.update(sketch.random(0,1));
            birds.add(bird);
        }

        predators = new ArrayList<Predator>();
        PVector randomPosition = new PVector(sketch.random(0,sketch.width), sketch.random(0,sketch.height));
        PImage predatorImage = sketch.loadImage("src\\main\\resources\\images\\predator.png");
        predators.add(new Predator(sketch,randomPosition,new Sprite(predatorImage,sketch)));
        randomPosition = new PVector(sketch.random(0,sketch.width), sketch.random(0,sketch.height));
        predators.add(new Predator(sketch,randomPosition,new Sprite(predatorImage,sketch)));

        obstacles = new ArrayList<Obstacle>();
        for(int i = 0; i < 3; i++)
        {
            obstacles.add(new Obstacle(sketch, new PVector(sketch.random(50,sketch.width),sketch.random(50,sketch.height))));
        }


    }

    /*
     * Clears all grid cells
     */
    public void empty() {
        for (ArrayList<Agent> cell : cells) {
            cell.clear();
        }
    }

    /*
     * Adds the bird to the appropriate grid cell
     */
    public void add(Agent b, float x, float y) {
        if (x < 0 || y < 0 || x >= sketch.width || y >= sketch.height) {
            throw new RuntimeException("Tried to add bird outside of grid bounds!!");
        }

        int cellX = (int)(x / cellSize);
        int cellY = (int)(y / cellSize);
        getCell(cellX, cellY).add(b);
    }

    /*
     * Returns a cell, based on its coordinates
     */
    public ArrayList<Agent> getCell(int cellX, int cellY) {
        int cellIndex = cellX+ cellY*numCellsX;
        return cells.get(cellIndex);
    }

    /*
     * Performs a broad-phase lookup of all birds which *might* be within the specified radius of the specified point.
     * This can have false-positives, so the results need to be filtered with a proper distance check afterwards
     */
    public ArrayList<Agent> query(float x, float y, float radius) {
        ArrayList<Agent> results = new ArrayList<Agent>(32);

        float minX = x-radius;
        float maxX = x+radius;
        float minY = y-radius;
        float maxY = y+radius;

        int minCellX = sketch.max(0, (int)(minX/cellSize));
        int maxCellX = sketch.min(numCellsX-1, (int)(maxX/cellSize));
        int minCellY = sketch.max(0, (int)(minY/cellSize));
        int maxCellY = sketch.min(numCellsY-1, (int)(maxY/cellSize));

        for (int cellX=minCellX; cellX<=maxCellX; ++cellX) {
            for (int cellY=minCellY; cellY<=maxCellY; ++cellY) {
                results.addAll(getCell(cellX, cellY));
            }
        }

        return results;
    }
}
