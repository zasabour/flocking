# FROM alpine/git as clone 
# WORKDIR /app
# RUN git clone https://github.com/ZakariaSabour/flocking.git

# FROM maven:3.5-jdk-14-alpine as build 
# WORKDIR /app
# COPY --from=clone /app/flocking /app 
# RUN mvn install


# FROM openjdk:14-jre-alpine
# WORKDIR /app
# COPY --from=build /app/target/flocking-1.0-SNAPSHOT.jar /app
# CMD ["java -jar flocking-1.0-SNAPSHOT.jar"]

#base image
FROM openjdk:11-jre-slim-buster
COPY ./target/flocking-1.0-SNAPSHOT.jar  ./
CMD ["java", "-jar", "flocking-1.0-SNAPSHOT.jar"]